package ru.injuktor.marketplace;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.Inet4Address;
import java.util.Scanner;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

import ru.injuktor.marketplace.client.Network;
import ru.injuktor.marketplace.packets.client.CPacketRegistration;
import ru.injuktor.marketplace.packets.client.CPacketUserAuth;

public class Marketplace 
{
	
    private static ScheduledExecutorService scheduler = Executors.newScheduledThreadPool(1);
    private static Network network;
	
	public static void main(String[] args) throws InterruptedException, IOException {
		network = new Network();
		Thread.sleep(1000);
	    scheduler.scheduleAtFixedRate(() -> {
	    	if(network == null || !network.channel.isOpen())
	    		network = new Network();
	    }, 1, 8, TimeUnit.SECONDS);
	    while(true)
	    {
	    	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
	    	String line = br.readLine();
	    	
	    	if(line.startsWith("reg"))
	    	{
	    		String[] arguments = line.split(" "); 		
	    		CPacketRegistration packetRegistration = new CPacketRegistration(arguments[1], arguments[2], arguments[3]);
	    		
	    		network.sendTCP(packetRegistration);
	    	}
	    	if(line.startsWith("auth"))
	    	{
	    		String[] arguments = line.split(" ");
	    		System.out.println(arguments[1] + "/" + arguments[2]);
	    		CPacketUserAuth packetAuth = new CPacketUserAuth(arguments[1], arguments[2]);
	    		
	    		network.sendTCP(packetAuth);
	    	}
	    }
	}
	
}
