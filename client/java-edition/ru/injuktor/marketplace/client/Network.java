package ru.injuktor.marketplace.client;

import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.Scanner;

import io.netty.bootstrap.Bootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelOption;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import ru.injuktor.marketplace.client.handlers.CAuthHandler;
import ru.injuktor.marketplace.client.handlers.CErrorHandler;
import ru.injuktor.marketplace.client.handlers.MainHandler;

public class Network {
    public SocketChannel channel;

    private static final String HOST = "127.0.0.1";
    private static final int PORT = 8189;

    public Network() {
        Thread t = new Thread(() -> {
            NioEventLoopGroup workerGroup = new NioEventLoopGroup(1);
            try {
                Bootstrap b = new Bootstrap();
                b.group(workerGroup)
                        .channel(NioSocketChannel.class)
                        .handler(new ChannelInitializer<SocketChannel>() {
                            @Override
                            protected void initChannel(SocketChannel socketChannel) throws Exception {
                                channel = socketChannel;
                                socketChannel.pipeline().addLast(new ObjectEncoder());
                                socketChannel.pipeline().addLast(new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
                                socketChannel.pipeline().addLast(new MainHandler());
                            }
                        })
                        .option(ChannelOption.SO_KEEPALIVE, true);
                ChannelFuture future = b.connect(HOST, PORT).sync();
                future.channel().closeFuture().sync();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                workerGroup.shutdownGracefully();
            }
        });
        t.start();
    }

    public void close() {
        channel.close();
    }
    
    public void sendTCP(Object object)
    {
    	channel.writeAndFlush(object);
    }
}