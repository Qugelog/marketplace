package ru.injuktor.marketplace.client.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import ru.injuktor.marketplace.packets.server.SPacketError;

public class CErrorHandler
{
	
	public static void channelRead(ChannelHandlerContext channelHandlerContext, Object msg) throws Exception 
	{
    	if(msg instanceof SPacketError)
    	{
    		SPacketError packetError = (SPacketError) msg;
    		
    		System.out.println(packetError.getError());
    	}
    }
	
}
