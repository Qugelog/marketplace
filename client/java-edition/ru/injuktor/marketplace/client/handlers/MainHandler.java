package ru.injuktor.marketplace.client.handlers;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;

public class MainHandler extends ChannelInboundHandlerAdapter
{
	@Override
	public void channelRead(ChannelHandlerContext channelHandlerContext, Object msg) throws Exception 
	{
		read(channelHandlerContext, msg);
    }
	
	private void read(ChannelHandlerContext channelHandlerContext, Object msg) throws Exception
	{
		CAuthHandler.channelRead(channelHandlerContext, msg);
		CErrorHandler.channelRead(channelHandlerContext, msg);
	}
}
