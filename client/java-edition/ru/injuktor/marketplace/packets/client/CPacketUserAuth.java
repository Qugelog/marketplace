package ru.injuktor.marketplace.packets.client;

import java.io.Serializable;

/**
 * Пакет авторизации от игрока.
 * 
 * @author mankrash
 *
 */

public class CPacketUserAuth implements Serializable 
{
	
	private String email;
	private String password;
	
	public CPacketUserAuth(String email, String password) {
		this.email 		= email;
		this.password 	= password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPassword() {
		return password;
	}
	
}
