package ru.injuktor.marketplace.packets.server;

import java.io.Serializable;

public class SPacketAuth implements Serializable
{
	
	private boolean allowed;
	
	public SPacketAuth(boolean allowed) {
		this.allowed = allowed;
	}
	
	public boolean isAllowed() {
		return allowed;
	}
	
}
