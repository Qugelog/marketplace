package ru.injuktor.marketplace.packets.server;

import java.io.Serializable;

import ru.injuktor.marketplace.server.objects.User;

public class SPacketUser implements Serializable
{
	
	private User user;
	
	public SPacketUser(User user) {
		this.user = user;
	}
	
	public User getUser() {
		return user;
	}
	
}
