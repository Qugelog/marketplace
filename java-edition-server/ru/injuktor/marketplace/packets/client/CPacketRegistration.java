package ru.injuktor.marketplace.packets.client;

import java.io.Serializable;

public class CPacketRegistration implements Serializable
{
	
	private String nickName;
	private String email;
	private String password;
	
	public CPacketRegistration(String nickName, String email, String password) {
		this.nickName = nickName;
		this.email = email;
		this.password = password;
	}
	
	public String getEmail() {
		return email;
	}
	
	public String getPassword() {
		return password;
	}
	
	public String getNickName() {
		return nickName;
	}
	
}
