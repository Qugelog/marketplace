package ru.injuktor.marketplace.packets.server;

import java.io.Serializable;

public class SPacketError implements Serializable
{
	
	private String error;
	
	public SPacketError(String error) {
		this.error = error;
	}
	
	public String getError() {
		return error;
	}
	
}
