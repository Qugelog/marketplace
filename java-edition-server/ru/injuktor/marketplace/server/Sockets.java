package ru.injuktor.marketplace.server;

import java.io.BufferedReader;
import java.io.InputStreamReader;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelFuture;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.EventLoopGroup;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.SocketChannel;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.handler.codec.serialization.ClassResolvers;
import io.netty.handler.codec.serialization.ObjectDecoder;
import io.netty.handler.codec.serialization.ObjectEncoder;
import ru.injuktor.marketplace.server.handlers.MainHandler;
import ru.injuktor.marketplace.server.handlers.SAuthHandler;
import ru.injuktor.marketplace.server.mysql.ConnectorManager;

public class Sockets 
{
	
	private static ConnectorManager connectorManager;
	
	public static void main(String[] args) {
		connectorManager = new ConnectorManager();
		
		EventLoopGroup bossGroup = new NioEventLoopGroup(1);
        EventLoopGroup workerGroup = new NioEventLoopGroup(1);
        try {
            ServerBootstrap b = new ServerBootstrap();
            b.group(bossGroup, workerGroup)
                    .channel(NioServerSocketChannel.class)
                    .childHandler(new ChannelInitializer<SocketChannel>() {
                        @Override
                        protected void initChannel(SocketChannel socketChannel) throws Exception {
                            socketChannel.pipeline().addLast(new ObjectEncoder());
                            socketChannel.pipeline().addLast(new ObjectDecoder(ClassResolvers.cacheDisabled(null)));
                            socketChannel.pipeline().addLast(new MainHandler());
                        }
                    });
            ChannelFuture future = b.bind(8189).sync();
            while(true) 
        	{
            	BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
        		String cmd = br.readLine();
        		
        		if(cmd.equalsIgnoreCase("stop"))
        		{
        			System.out.println("Сервер прекращает работу.");
        			future.channel().close();
        			System.exit(0);
        		}
        	}
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossGroup.shutdownGracefully();
            workerGroup.shutdownGracefully();
        }
	}
	
	public static long hash(String nick, String pass, long time, int id)
	{
		long sum = 0;
		
		int toMultiplier = 0;
		
		if(nick.length() >= 1 && nick.length() < 5)
			toMultiplier = 2;
		if(nick.length() >= 5 && nick.length() < 10)
			toMultiplier = 4;
		if(nick.length() >= 10)
			toMultiplier = 6;
		
		int startMulti = toMultiplier * nick.length();
		
		long sumNick = 0;
		
		for(char symbolInNick : nick.toCharArray())
		{
			sumNick = ((long) symbolInNick);
		}
		
		for(char symbol : pass.toCharArray())
		{
			sum = (((long) symbol) * startMulti) * id;
			
			startMulti /= 2;
			if(startMulti <= 1)
			{
				startMulti = toMultiplier * nick.length();
			}
		}
		
		if(nick.matches("A-Z"))
			sum *= 1 + (0.12759412515341211 * id);
		
		if(pass.matches("A-Z"))
			sum *= 1 + (0.35764234181252 * id);
		
		sum += sumNick * time;
		
		return sum;
	}
	
	public static boolean dehash(String nick, long hashPass, String readPass, long time, int id)
	{
		if(hash(nick, readPass, time, id) == hashPass)
		{
			return true;
		}
		return false;
	}
	
	public static ConnectorManager getConnectorManager() {
		return connectorManager;
	}
	
}
