package ru.injuktor.marketplace.server.handlers;

import java.net.InetSocketAddress;
import java.sql.ResultSet;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import ru.injuktor.marketplace.packets.client.CPacketRegistration;
import ru.injuktor.marketplace.packets.client.CPacketUserAuth;
import ru.injuktor.marketplace.packets.server.SPacketAuth;
import ru.injuktor.marketplace.packets.server.SPacketError;
import ru.injuktor.marketplace.packets.server.SPacketUser;
import ru.injuktor.marketplace.server.objects.Client;
import ru.injuktor.marketplace.server.objects.User;

public class MainHandler extends ChannelInboundHandlerAdapter
{
	
	public static int indexClient;
	
	@Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        ++indexClient;
        System.out.println("Клиент подключился: " + ctx);
        
        new Client(ctx.channel());        
    }
	
	@Override
    public void channelRead(ChannelHandlerContext ctx, Object p) throws Exception {
		listener(ctx, p);
	}
	
	private void listener(ChannelHandlerContext ctx, Object p) throws Exception
	{
		new SAuthHandler().channelRead(ctx, p);
	}
	
	@Override
    public void channelInactive(ChannelHandlerContext ctx) throws Exception {
        Client client = Client.getClient(ctx.channel());
        client.unload();
        ctx.close();
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
    	Client client = Client.getClient(ctx.channel());
        client.unload();
        ctx.close();
    }
}
