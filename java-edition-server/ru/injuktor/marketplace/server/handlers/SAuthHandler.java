package ru.injuktor.marketplace.server.handlers;

import java.net.InetSocketAddress;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;
import java.util.Date;

import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.ChannelInboundHandlerAdapter;
import ru.injuktor.marketplace.packets.client.CPacketRegistration;
import ru.injuktor.marketplace.packets.client.CPacketUserAuth;
import ru.injuktor.marketplace.packets.server.SPacketAuth;
import ru.injuktor.marketplace.packets.server.SPacketError;
import ru.injuktor.marketplace.packets.server.SPacketUser;
import ru.injuktor.marketplace.server.Sockets;
import ru.injuktor.marketplace.server.objects.Client;
import ru.injuktor.marketplace.server.objects.User;

public class SAuthHandler
{
	
    public void channelRead(ChannelHandlerContext ctx, Object p) throws Exception {
		Client client = Client.getClient(ctx.channel());
		if(p instanceof CPacketUserAuth)
		{
			CPacketUserAuth packet = (CPacketUserAuth) p;
			
			String emailClient = packet.getEmail();
			String passwordClient = packet.getPassword();
			
			
			ResultSet resultRegistration = Sockets.getConnectorManager().query("SELECT * FROM `log_registration` WHERE email='" + emailClient + "'");
			ResultSet result = Sockets.getConnectorManager().query("SELECT * FROM `users` WHERE email='" + emailClient + "'");
			
			if(result.next() && resultRegistration.next())
			{
				String nickName = result.getString("nickname");
				
				int regId = resultRegistration.getInt("id");
				long regTime = resultRegistration.getTimestamp("time").getTime();
				String passwordServer = result.getString("password");
				long hashPasswordServer = Long.valueOf(passwordServer);
				
				System.out.println(hashPasswordServer + "/" + Sockets.hash(nickName, passwordClient, regTime, regId));
				
				if(Sockets.dehash(nickName, hashPasswordServer, passwordClient, regTime, regId))
				{
					int id = result.getInt("id");
					int balance = result.getInt("balance");
					float rating = result.getFloat("rating");
					
					User user = new User(id, nickName, emailClient, balance, rating);
					SPacketUser packetUser = new SPacketUser(user);
					
					System.out.println("12345");
					
					client.sendTCP(packetUser);
					
					SPacketError packetError = new SPacketError("hey");
					
					client.sendTCP(packetError);
				} else {
					SPacketError packetError = new SPacketError("Пароль или логин неверный.");
					
					client.sendTCP(packetError);
					SPacketAuth packetAuth = new SPacketAuth(false);
					
					client.sendTCP(packetAuth);
				}
			} else {
				SPacketError packetError = new SPacketError("Аккаунт не найден или отстуствует подключение к БД.");
				
				client.sendTCP(packetError);
			}
		}
		if(p instanceof CPacketRegistration)
		{
			CPacketRegistration packetRegistration = (CPacketRegistration) p;
			String email = packetRegistration.getEmail();
			String nickName = packetRegistration.getNickName();
			String password = packetRegistration.getPassword();
						
			ResultSet resultRegistration = Sockets.getConnectorManager().query("SELECT * FROM `log_registration` WHERE email='" + email + "'");
			
			if(!resultRegistration.next())
			{
				Date date = new java.util.Date(System.currentTimeMillis());
				
				SimpleDateFormat sdf = new java.text.SimpleDateFormat("yyyy-MM-dd HH:mm:ss"); 
				String formattedDate = sdf.format(date);
				
				String insert = String.format("INSERT INTO `log_registration`(`name`, `email`, `ip`, `time`) VALUES ('%s', '%s', '%s', '%s');", nickName, 
						email, ((InetSocketAddress) ctx.channel().remoteAddress()).getAddress().getHostAddress(), formattedDate);
				
				Sockets.getConnectorManager().query(insert);
				
				ResultSet result = Sockets.getConnectorManager().query("SELECT * FROM `log_registration` WHERE email='" + email + "'");
				if(result.next())
				{
					int regId = result.getInt("id");
					long regTime = result.getTimestamp("time").getTime();
					
					long hashPass = Sockets.hash(nickName, password, regTime, regId);
					
					Sockets.getConnectorManager().query(String.format("INSERT INTO `users`(`nickname`, `email`, `password`, `last_auth`, `balance`, `rating`) VALUES "
							+ "('" + nickName + "', '" + email + "', '" + hashPass + "', '" + formattedDate + "', '0', '0')"));
				} else {
					SPacketError packetError = new SPacketError("Ошибка регистрации на стороне сервера");
					
					client.sendTCP(packetError);
				}
				
			} else {
				SPacketError packetError = new SPacketError("Аккаунт с такой почтой уже существует!");
				
				client.sendTCP(packetError);
			}
		}
	}
	
}
