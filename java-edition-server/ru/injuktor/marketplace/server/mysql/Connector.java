package ru.injuktor.marketplace.server.mysql;

import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.LinkedList;
import java.util.List;
import java.util.concurrent.BlockingQueue;

public class Connector implements Runnable {
    private static List<Connector> list;

    static {
        Connector.list = new LinkedList<Connector>();
    }

    private String name;
    private String user;
    private String pass;
    private String url;
    private Connection connection;
    private boolean interrupted;
    @SuppressWarnings("unused")
	private boolean queryInProgress;
    private boolean queryAddEnabled;
    private BlockingQueue<String> queryQueue;

    public Connector(Connection connection) {
    	this.connection = connection;
    }

    public static void shutdownAll() {
        for (final Connector c : Connector.list) {
            c.shutdown();
            c.close();
            c.interrupt();
        }
    }

    public boolean addToQuery(final String query) {
        if (queryAddEnabled) {
            return this.queryQueue.offer(query);
        }
        throw new IllegalStateException("This connector isn't accepting queries");
    }

    public boolean addToQuery(final String query, final Object... args) {
        return (boolean) this.addToQuery(String.format(query, args));
    }

    @Override
    public void run() {
        try {
            while (!this.interrupted) {
                queryInProgress = true;
                final String query = this.queryQueue.take();
                this.query(query);
                queryInProgress = false;
            }
        } catch (InterruptedException ex) {
        }
    }

    public String getName() {
        return this.name;
    }

    public void interrupt() {
        this.interrupted = true;
    }

    public boolean shutdown() {
        queryInProgress = false;
        synchronized (Connector.class) {
            while (!this.queryQueue.isEmpty()) {
                try {
                    Thread.sleep(500L);
                } catch (InterruptedException ex) {
                }
            }
        }
        return true;
    }

    private boolean checkDriver() {
        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            return true;
        } catch (ClassNotFoundException e) {
            return false;
        }
    }

    public boolean checkConnection() {
        return this.open() != null;
    }

    public Connection open() {
        if (!this.checkDriver()) {
            return null;
        }
        try {
            if (this.connection == null) {
                return DriverManager.getConnection(this.url, this.user, this.pass);
            }
            if (this.connection.isValid(0)) {
                return this.connection;
            }
            return DriverManager.getConnection(this.url, this.user, this.pass);
        } catch (SQLException e) {
            return null;
        }
    }

    public boolean close() {
        this.connection = this.open();
        try {
            if (this.connection != null) {
                this.connection.close();
                this.connection = null;
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public ResultSet query(final String query, final Object... args) {
        return this.query(String.format(query, args));
    }

    public ResultSet query(final String query) {
        Statement statement = null;
        ResultSet result = null;
        queryInProgress = true;
        try {
            this.connection = this.open();
            statement = this.connection.createStatement();
            if (this.getStatement(query).equals(Statements.SELECT)) {
                result = statement.executeQuery(query);
                queryInProgress = false;
                return result;
            }
            if (this.getStatement(query).equals(Statements.INSERT)) {
                statement.executeUpdate(query);
                queryInProgress = false;
                return result;
            }
            statement.executeUpdate(query);
            queryInProgress = false;
            return result;
        } catch (SQLException e) {
            e.printStackTrace();
            queryInProgress = false;
            return result;
        }
    }

    public int updateQuery(final String query) {
        Connection connection = null;
        Statement statement = null;
        try {
            connection = this.open();
            statement = connection.createStatement();
            return statement.executeUpdate(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return 0;
        }
    }

    public PreparedStatement prepare(final String query) {
        Connection connection = null;
        final PreparedStatement ps = null;
        try {
            connection = this.open();
            return connection.prepareStatement(query);
        } catch (SQLException e) {
            e.printStackTrace();
            return ps;
        }
    }

    public boolean createTable(final String query) {
        Statement statement = null;
        try {
            this.connection = this.open();
            statement = this.connection.createStatement();
            statement.execute(query);
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
            return false;
        } catch (Exception e2) {
            e2.printStackTrace();
            return false;
        }
    }

    public boolean checkTable(final String table) {
        try {
            this.connection = this.open();
            final Statement statement = this.connection.createStatement();
            final ResultSet result = statement.executeQuery("SELECT * FROM " + table);
            if (result == null) {
                return false;
            }
        } catch (SQLException e) {
            if (e.getMessage().contains("exist")) {
                return false;
            }
            if (this.query("SELECT * FROM " + table) == null) {
                return true;
            }
        }
        return false;
    }

    public boolean colExists(final String table, final String column) {
        try {
            this.connection = this.open();
            if (this.connection == null) {
                return false;
            }
            final DatabaseMetaData metadata = this.connection.getMetaData();
            final ResultSet result = metadata.getColumns(null, null, table, column);
            if (result == null) {
                return false;
            }
            result.next();
            return true;
        } catch (SQLException e) {
            if (e.getMessage().contains("exist")) {
                return false;
            }
            e.printStackTrace();
            return false;
        }
    }

    protected Statements getStatement(final String query) {
        final String trimmedQuery = query.trim();
        if (trimmedQuery.substring(0, 6).equalsIgnoreCase("SELECT"))
            return Statements.SELECT;
        if (trimmedQuery.substring(0, 6).equalsIgnoreCase("INSERT"))
            return Statements.INSERT;
        if (trimmedQuery.substring(0, 6).equalsIgnoreCase("UPDATE"))
            return Statements.UPDATE;
        if (trimmedQuery.substring(0, 6).equalsIgnoreCase("DELETE"))
            return Statements.DELETE;
        if (trimmedQuery.substring(0, 6).equalsIgnoreCase("CREATE"))
            return Statements.CREATE;
        if (trimmedQuery.substring(0, 5).equalsIgnoreCase("ALTER"))
            return Statements.ALTER;
        if (trimmedQuery.substring(0, 4).equalsIgnoreCase("DROP"))
            return Statements.DROP;
        if (trimmedQuery.substring(0, 8).equalsIgnoreCase("TRUNCATE"))
            return Statements.TRUNCATE;
        if (trimmedQuery.substring(0, 6).equalsIgnoreCase("RENAME"))
            return Statements.RENAME;
        if (trimmedQuery.substring(0, 2).equalsIgnoreCase("DO"))
            return Statements.DO;
        if (trimmedQuery.substring(0, 7).equalsIgnoreCase("REPLACE"))
            return Statements.REPLACE;
        if (trimmedQuery.substring(0, 4).equalsIgnoreCase("LOAD"))
            return Statements.LOAD;
        if (trimmedQuery.substring(0, 7).equalsIgnoreCase("HANDLER"))
            return Statements.HANDLER;
        if (trimmedQuery.substring(0, 4).equalsIgnoreCase("CALL"))
            return Statements.CALL;
        return Statements.SELECT;
    }

    protected enum Statements {
        SELECT,
        INSERT,
        UPDATE,
        DELETE,
        DO,
        REPLACE,
        LOAD,
        HANDLER,
        CALL,
        CREATE,
        ALTER,
        DROP,
        TRUNCATE,
        RENAME;
    }
}
