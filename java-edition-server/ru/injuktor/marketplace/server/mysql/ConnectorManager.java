package ru.injuktor.marketplace.server.mysql;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Работа с БД
 * 
 * @author mankrash
 *
 */

public class ConnectorManager 
{
	
	private Connector connector;
	private Connection connection;
	
	public ConnectorManager() {
		try {
			connect();
		} catch (SQLException e) {
			e.printStackTrace();
		}
	}
	
	public void registration(String user, String password, String ip)
	{
		
	}
	
	public ResultSet query(String query)
	{
		return getConnector().query(query);
	}
	
	private String getUrl(String ip, String db)
	{
		return String.format("jdbc:mysql://%s:3306/%s?autoReconnect=true&useSSL=false&useLegacyDatetimeCode=false&serverTimezone=UTC", ip, db);
	}
	
	public void connect() throws SQLException
	{
		connection = DriverManager.getConnection(getUrl("46.105.122.17", "marketplace"), "marketplace", "nV2vW3sO");
         if (!connection.isClosed())
             System.out.println("Соединение установлено: marketplace");
         else
             System.out.println("Ошибка соединения: marketplace");
         connector = new Connector(connection);
	}
	
	public Connector getConnector()
	{
		return connector;
	}
	
}
