package ru.injuktor.marketplace.server.objects;

import java.util.HashMap;

import io.netty.channel.Channel;

public class Client 
{
	
	private Channel channel;
	
	private static HashMap<Channel, Client> clients = new HashMap<>();
	
	public Client(Channel channel) {
		this.channel = channel;
		
		clients.put(channel, this);
	}
	
	public static Client getClient(Channel channel)
	{
		return clients.get(channel);
	}
	
	public void unload()
	{
		clients.remove(channel);
	}
	
	public Channel getChannel() {
		return channel;
	}
	
	public void sendTCP(Object object)
	{
		channel.writeAndFlush(object);
	}
	
}
