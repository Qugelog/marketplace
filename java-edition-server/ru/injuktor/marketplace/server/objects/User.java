package ru.injuktor.marketplace.server.objects;

import java.io.Serializable;
import java.util.HashMap;

public class User implements Serializable
{
	
	private int id;
	private String nickname;
	private String email;
	private int balance;
	private float rating;
	
	private static HashMap<Integer, User> users = new HashMap<Integer, User>();
	
	public User(int id, String nickname, String email, int balance, float rating) {
		this.id 		= id;
		this.nickname 	= nickname;
		this.email 		= email;
		this.balance    = balance;
		this.rating 	= rating;
		
		users.put(id, this);
	}
	
	public int getId() {
		return id;
	}
	
	public String getNickname() {
		return nickname;
	}
	
	public String getEmail() {
		return email;
	}
	
	public int getBalance() {
		return balance;
	}
	
	public float getRating() {
		return rating;
	}
	
	public void setBalance(int balance) {
		this.balance = balance;
	}
	
	public void setRating(float rating) {
		this.rating = rating;
	}
	
	public void unload()
	{
		users.remove(id);
	}
	
}
